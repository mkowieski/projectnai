#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>

using namespace cv;
using namespace std;

string windowName = "Projekt by Mirosław Kowieski";

string faceCascadeFile = "cascades/face/haarcascade_frontalface_alt.xml";
string profileFaceCascadeFile = "cascades/face/haarcascade_profileface.xml";
string eyeCascadeFile = "cascades/eye/haarcascade_eye_tree_eyeglasses.xml";
string noseCascadeFile = "cascades/nose/haarcascade_mcs_nose.xml";
string mouthCascadeFile = "cascades/mouth/haarcascade_mcs_mouth.xml";
string leftearCascadeFile = "cascades/ear/haarcascade_mcs_leftear.xml";
string rightearCascadeFile = "cascades/ear/haarcascade_mcs_rightear.xml";

CascadeClassifier faceCascade, profileFaceCascade, eyeCascade, noseCascade, mouthCascade, leftearCascade, rightearCascade;

vector<Rect> face, profile_face, eye, nose, mouth, leftear, rightear;

void detectLeftear(Mat frame){
	leftearCascade.detectMultiScale(frame, leftear, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(40, 40)); 
	for( unsigned i = 0; i < leftear.size(); i++ ) 
	{ 
		Rect leftearDetected(leftear[i]); 
		rectangle(frame, leftearDetected, Scalar( 56, 23, 129 ), 2, 2, 0  ); 
	}
}

void detectRightear(Mat frame){
	rightearCascade.detectMultiScale(frame, rightear, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(40, 40)); 
	for( unsigned i = 0; i < rightear.size(); i++ ) 
	{ 
		Rect rightearDetected(leftear[i]); 
		rectangle(frame, rightearDetected, Scalar( 56, 23, 129 ), 2, 2, 0  ); 
	}
}

void detectNose(Mat frame){
	noseCascade.detectMultiScale( frame, nose, 1.1, 2,  0 |CV_HAAR_SCALE_IMAGE, Size(40, 40) ); 
	for( unsigned i = 0; i < nose.size(); i++ ) 
	{ 
		Rect noseDetected(nose[i]); 
		rectangle(frame, noseDetected, Scalar( 100, 100, 129 ), 2, 2, 0  ); 
	}
}

void detectMouth(Mat frame){
	mouthCascade.detectMultiScale(frame, mouth, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(40, 40)); 
	for( unsigned i = 0; i < mouth.size(); i++ ) 
	{ 
		Rect mouthDetected(mouth[i]); 
		rectangle(frame, mouthDetected, Scalar( 56, 23, 129 ), 2, 2, 0  ); 
	}
}

void detectFace(Mat frame) {
	faceCascade.detectMultiScale(frame, face, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(55, 55));
	for(unsigned i = 0; i < face.size(); i++) {
		Rect faceDetected(face[i]);
		rectangle(frame, faceDetected, Scalar(255,0,0), 2,2,0);
     }
}

void detectProfileFace(Mat frame) {
	profileFaceCascade.detectMultiScale(frame, profile_face, 1.1, 3, 0|CV_HAAR_SCALE_IMAGE, Size(55, 55));
	for(unsigned i = 0; i < profile_face.size(); i++) {
		Rect profileFaceDetected(profile_face[i]);
		rectangle(frame, profileFaceDetected, Scalar(160,0,200), 2,2,0);
     }
}

void detectEye(Mat frame) {
	eyeCascade.detectMultiScale(frame, eye, 1.1, 2,  0 |CV_HAAR_SCALE_IMAGE, Size(30, 30) ); 
	for(unsigned i = 0; i < eye.size(); i++) {
		Rect eyeDetected(eye[i]);
		rectangle(frame, eyeDetected, Scalar(160,0,200), 2,2,0);
     }
}

void alarmTextRed(Mat frame, string text) {
	int fontFace = CV_FONT_HERSHEY_SIMPLEX;
	double fontScale = 1;
	int thickness = 2;  
	
	Point textPosition(10, 50);
	putText(frame, text, textPosition, fontFace, fontScale, Scalar(0,0,255), thickness, 8);
}

void alarmTextWhite(Mat frame, string text) {
	int fontFace = CV_FONT_HERSHEY_SIMPLEX;
	double fontScale = 1;
	int thickness = 2;  
	
	Point textPosition(10, 50);
	putText(frame, text, textPosition, fontFace, fontScale, Scalar(255,255,255), thickness, 8);
}

void alarmTextRedTest(Mat frame, string text) {
	int fontFace = CV_FONT_HERSHEY_SIMPLEX;
	double fontScale = 1;
	int thickness = 2;  
	
	Point textPosition(10, 100);
	putText(frame, text, textPosition, fontFace, fontScale, Scalar(0,0,255), thickness, 8);
}

int main(int argc, char** argv) {
	
	bool continueCapture = true;
	Mat frame;


	if (!faceCascade.load(faceCascadeFile)) {
		cout << "Nie znaleziono pliku: " << faceCascadeFile << ".";
		return -1;
	} else cout << "Wczytano plik poprawnie: " << faceCascadeFile << "." << endl;
	
	if (!profileFaceCascade.load(profileFaceCascadeFile)) {
		cout << "Nie znaleziono pliku: " << profileFaceCascadeFile << ".";
		return -1;
	} else cout << "Wczytano plik poprawnie: " << profileFaceCascadeFile << "." << endl;
	
	if (!eyeCascade.load(eyeCascadeFile)) {
		cout << "Nie znaleziono pliku: " << eyeCascadeFile << ".";
		return -1;
	} else cout << "Wczytano plik poprawnie: " << eyeCascadeFile << "." << endl;
	
	if (!noseCascade.load(noseCascadeFile)) {
		cout << "Nie znaleziono pliku: " << noseCascadeFile << ".";
		return -1;
	} else cout << "Wczytano plik poprawnie: " << noseCascadeFile << "." << endl;
	
	if (!mouthCascade.load(mouthCascadeFile)) {
		cout << "Nie znaleziono pliku: " << mouthCascadeFile << ".";
		return -1;
	} else cout << "Wczytano plik poprawnie: " << mouthCascadeFile << "." << endl;
	
	if (!leftearCascade.load(leftearCascadeFile)) {
		cout << "Nie znaleziono pliku: " << leftearCascadeFile << ".";
		return -1;
	} else cout << "Wczytano plik poprawnie: " << leftearCascadeFile << "." << endl;
	
	if (!rightearCascade.load(rightearCascadeFile)) {
		cout << "Nie znaleziono pliku: " << rightearCascadeFile << ".";
		return -1;
	} else cout << "Wczytano plik poprawnie: " << rightearCascadeFile << "." << endl;

	
    namedWindow(windowName, CV_WINDOW_AUTOSIZE);
    
    VideoCapture cap(0);
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
   
    if (!cap.isOpened()) return -1;
    double w = cap.get(CV_CAP_PROP_FRAME_WIDTH);
	double h = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
	double fps = cap.get(CV_CAP_PROP_FPS);
	cout << "\n\n";
	cout << "__________________ INFORMACJE _________________" << endl;
	cout << "|                                             |" << endl;
	cout << "     Rozdzielczość wideo: " << w << " x " << h << endl;
	cout << "     Ilość klatek: " << fps << " fps" << endl;
	cout << "_______________________________________________" << endl;
	cout << "\n\n";
	
    while (continueCapture) {
		Mat mirror;
		Mat detected;
		char test;
		
		if (cap.read(frame)) {
			
			detectFace(frame);
			detectProfileFace(frame);
			detectEye(frame);
			detectNose(frame);
			detectMouth(frame);
			detectLeftear(frame);
			detectRightear(frame);
			
			flip(frame, mirror, 1);
			
			
			if(face.empty() && profile_face.empty() && eye.empty() && nose.empty() && mouth.empty() && leftear.empty() && rightear.empty()) {
				alarmTextWhite(mirror, "nie znaleziono twarzy.");
			} else {
				alarmTextRed(mirror, "WYKRYTE !");
				flip(frame, detected, 1);
				imwrite("twarz.jpg", detected);
			}
			
			imshow(windowName, mirror);
			
		} else continueCapture = false;
	
		if(waitKey(10) >= 0) continueCapture = false;
	}
	
    return 0;
}



